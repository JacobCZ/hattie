package cz.sycha.hattie;

import java.util.logging.Logger;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.plugin.java.JavaPlugin;

public class Main
  extends JavaPlugin
{
  protected final Logger log = Logger.getLogger("minecraft");
  
  public void onEnable()
  {
    this.log.info("[Hattie] - Plugin enabled!");
  }
  
  public void onDisable()
  {
    this.log.info("[Hattie] - Plugin disabled!");
  }
  
  public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
  {
    if (!(sender instanceof Player))
    {
      sender.sendMessage(ChatColor.RED + "This command can only be used by players!");
      return false;
    }
    Player player = (Player)sender;
    if ((cmd.getName().equalsIgnoreCase("hat")) && 
      (player.hasPermission("hattie.hat")))
    {
      PlayerInventory inv = player.getInventory();
      ItemStack item = player.getItemInHand();
      ItemStack oldItem = inv.getHelmet();
      
      inv.setHelmet(item);
      inv.remove(item);
      inv.addItem(new ItemStack[] { oldItem });
      
      player.sendMessage(ChatColor.DARK_AQUA + "The selected item was put on your head!");
      
      return true;
    }
    return false;
  }
}
